const cardsContainer = document.querySelector(".cards");
const movesSpan = document.getElementById('moves');
const startButton = document.getElementById('Start');
const timeSpan = document.getElementById('time');

const emojis = ['😀', '🐶', '🌸', '🍕', '🚀', '🎈', '🐱', '🍦'];
const emojisPickList = emojis.concat(emojis);
const cardsCount = emojisPickList.length;

let flippedCards = [];
let moves = 0;
let gameStarted = false;
let timerInterval;
let matchedEmojis = [];

startButton.addEventListener('click', () => {
    if (!gameStarted) {
        gameStarted = true;

        startButton.style.backgroundColor = 'grey';
        startButton.style.color = 'black';

        let seconds = 0;

        timerInterval = setInterval(() => {
            seconds += 1;
            timeSpan.textContent = `time: ${seconds} sec`;
        }, 1000);
    };
});

for (let index = 0; index < cardsCount; index++) {
    const randomIndex = Math.floor(Math.random() * emojisPickList.length);
    const emoji = emojisPickList[randomIndex];
    const card = buildCard(emoji);

    emojisPickList.splice(randomIndex, 1);
    cardsContainer.appendChild(card);
};

function buildCard(emoji) {
    const element = document.createElement('div');

    element.classList.add('card');
    element.setAttribute('data-emoji', emoji);

    element.addEventListener('click', () => {
        if (gameStarted && flippedCards.length < 2 && !flippedCards.includes(element) && !matchedEmojis.includes(emoji)) {
            element.textContent = emoji;
            element.classList.add('revealed');
            flippedCards.push(element);
            moves += 1;
            movesSpan.textContent = `${moves} moves`;

            if (flippedCards.length === 2) {
                setTimeout(checkForMatch, 500);
            };
        };
    });

    return element;
};

function checkForMatch() {
    const [card1, card2] = flippedCards;
    const emoji1 = card1.getAttribute('data-emoji');
    const emoji2 = card2.getAttribute('data-emoji');

    if (emoji1 === emoji2) {
        flippedCards = [];
        matchedEmojis.push(emoji1);

        const matchedCards = document.querySelectorAll('.revealed');
        if (matchedCards.length === cardsCount) {
            clearInterval(timerInterval);
        }
    } else {
        setTimeout(() => {
            card1.classList.remove('revealed');
            card2.classList.remove('revealed');
            card1.textContent = '';
            card2.textContent = '';
            flippedCards = [];
        }, 500);
    };
};






